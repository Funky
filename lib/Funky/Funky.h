/* Funky: a light-weight embeddable programming language
 * Copyright (c) 2007, Ronald Landheer-Cieslak
 * All rights reserved
 * 
 * This is free software. You may distribute it and/or modify it and
 * distribute modified forms provided that the following terms are met:
 *
 * * Redistributions of the source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer;
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution;
 * * None of the names of the authors of this software may be used to endorse
 *   or promote this software, derived software or any distribution of this 
 *   software or any distribution of which this software is part, without 
 *   prior written permission from the authors involved;
 * * Unless you have received a written statement from Ronald Landheer-Cieslak
 *   that says otherwise, the terms of the GNU General Public License, as 
 *   published by the Free Software Foundation, version 2 or (at your option)
 *   any later version, also apply.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _funky_funky_h
#define _funky_funky_h

#include "Details/prologue.h"

#include <string>
#include <vector>
#include <boost/function.hpp>

namespace Funky
{
	namespace Private
	{
		template < typename LiteralType >
		struct Grammar;
	}
	/** Funky: a tiny, embeddable functional programming language. 
	 * This class implements a tiny, embeddable programming language that has 
	 * the following features:
	 * \li you can install your own functions in the language
	 * \li simple arithmatic functions are built-in
	 * \li you can pass any number of arguments to the script 
	 * The languages syntax is very lisp-like: a statement is written in brackets
	 * and consists of a comma-separated list, the first entry in which is a 
	 * function name (all others being its arguments). The following functions
	 * are built-in:
	 * \li test: returns 1 if its argument is non-zero, 0 otherwise.
	 * \li or: returns 1 if either of its two arguments is non-zero, 0 otherwise.
	 * \li and: returns 1 if both of its arguments are non-zero, 0 otherwise
	 * \li not: returns 1 if its argument is 0, 0 otherwise
	 * \li add: returns the sum of its two arguments
	 * \li neg: returns the negative equivalent of its argument
	 * \li mul: returns the product of its two arguments
	 * \li div: returns the result of dividing its two arguments
	 * For example, the following script:
	 * \code
	 * (add, (neg, (add, 6, 6)), 12)
	 * \endcode
	 * will return 0: it will first add 6 to 6, resulting in 12. It will then 
	 * negate 12, resulting in -12, to which it will add 12, resulting in 0.
	 *
	 * You may have noticed that there is no "sub" function built-in. That's because
	 * you don't need one: you can easily subtract like this: (add, \@0, (neg, \@1))
	 * which very nicely introduces two features of this little language: there's
	 * nothing there you don't need (but some things you do need may be missing - 
	 * for which reason you can always add to the language) and you can pass 
	 * parameters to your scripts and reference them using the @ sign and the index
	 * of the parameter you want in the parameters you've passed, starting at 0.
	 */
	class FUNKY_API Funky
	{
	public :
		//! The type of the arguments as they are passed around
		typedef std::vector< double > Arguments;
		//! Signature of an installable function: return a double and take a vector of doubles as parameter
		typedef boost::function1< double, std::vector< double > > Function;
		//! Signature of an installable function that returns more than one value
		typedef boost::function1< std::vector< double >, std::vector< double > > MultiReturnFunction;
		enum { any__ = -1 };

		//! Construct a interpreter without any functions installed (except the built-in ones)
		Funky();
		//! Destroy the interpreter and free all associated resources
		~Funky();

		/** Parse a given script for later evaluation.
		 * Once successfully parsed, you can use the run function to run the 
		 * script. Both steps can be done in a single go using the eval 
		 * function, which will parse and then run the script with the given
		 * arguments.
		 * note that parsing a script that defines functions will alter the
		 * state of the interpreter in that it will add functions to the 
		 * internal function table, and those functions may replace the ones
		 * already there (so if you want to override functions define in the
		 * scripts, you should install them after parsing the script. Likewise
		 * if you want a script to be able to override your built-in functions,
		 * install your functions before parsing the script).
		 *
		 * \param program_text the script to parse
		 * \throws Exceptions::ParseError if an error occurs during parsing */
		void parse(const std::string & program_text);

		/** Run a previously parsed script. Note that execution will start at
		 * the last statement parsed by the parser.
		 *
		 * \param arguments the arguments to pass to the script.
		 * \throws Exceptions::FunctionCallError if one of the functions does 
		 *         not behave as expected */
		double run(const Arguments & arguments = Arguments()) const;

		/** Evaluate a given script with the given arguments.
		 * \param program_text the script to evaluate
		 * \param arguments the arguments to pass to the script
		 * \return the result of the script
		 * \throws Exceptions::ParseError if an error occurs during parsing
		 * \throws Exceptions::FunctionCallError if one of the functions does 
		 *         not behave as expected */
		double eval(const std::string & program_text, const Arguments & arguments = Arguments());

		/** Install a function in the interpreter, callable by a script.
		 * \param function_name the name of the function to install
		 * \param function a pointer to the function to call when invoked by the script
		 * \param expected_args the expected number of arguments the function can be called with, or -1 if any amount is allowed */
		void installFunction(const std::string & function_name, const Function & function, int expected_args = any__);
		/** Install a function in the interpreter, callable by a script.
		 * \param function_name the name of the function to install
		 * \param function a pointer to the function to call when invoked by the script
		 * \param expected_args the expected number of arguments the function can be called with, or -1 if any amount is allowed
		 * \param expected_returns the number of values the function should be expected to return, or -1 if any amount is allowed */
		void installFunction(const std::string & function_name, const MultiReturnFunction & function, int expected_args = any__, int expected_returns = any__);
		/** Remove (uninstall) a previously installed function from the interpreter.
		 * \param function_name the function to remove */
		void removeFunction(const std::string & function_name);

	private :
		struct FunctionInfo;
		struct Data;

		// Neither CopyConstructible nor Assignable
		Funky(const Funky &);
		Funky &operator=(const Funky &);

		Data * data_;
		Private::Grammar< double > * grammar_;
	};
}

#endif
