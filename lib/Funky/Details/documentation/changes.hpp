/* Funky: a light-weight embeddable programming language
 * Copyright (c) 2007, Ronald Landheer-Cieslak
 * All rights reserved
 * 
 * This is free software. You may distribute it and/or modify it and
 * distribute modified forms provided that the following terms are met:
 *
 * * Redistributions of the source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer;
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution;
 * * None of the names of the authors of this software may be used to endorse
 *   or promote this software, derived software or any distribution of this 
 *   software or any distribution of which this software is part, without 
 *   prior written permission from the authors involved;
 * * Unless you have received a written statement from Ronald Landheer-Cieslak
 *   that says otherwise, the terms of the GNU General Public License, as 
 *   published by the Free Software Foundation, version 2 or (at your option)
 *   any later version, also apply.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/** \page changelog Changelog
 * <table>
 * <tr><th>Version</th><th>Changes</th></tr>
 * <tr><td>1.2.01 (3.1.3)</td><td>Add documentation on how to build Funky\n
 *                       Fix building with G++ 4.x</td></tr>
 * <tr><td>1.2.00 (3.0.3)</td><td>Re-factor the code of the interpreter to allow
 *                       for easy declaration/definition of front-ends with other
 *                       built-in types than floating-points and create a front-end
 *                       with integers as built-in types (Groovy)</td></tr>
 * <tr><td>1.1.01 (2.0.2)</td><td>Add the ParseError class and diagnose parse errors
 *                       more specifically than just diagnosing that everything
 *                       wasn't parsed. This makes the parser an assertive one,
 *                       for as far as it can be.\n
 *                       As the thrown exceptions are still derived from <code>
 *                       std::runtime_error</code> I'll consider the API to
 *                       still be compatible.
 *                       </td></tr>
 * <tr><td>1.1.00 (1.0.1)</td><td>Add support for functions returning more than one 
 *                       value, at least among the embedded functions (no 
 *                       grammar changes have been made for this).\n
 *                       This makes functions returning only one value a
 *                       special case and paves the way to making a version
 *                       of Funky that uses an integer as its built-in type.
 *                       </td></tr>
 * <tr><td>1.0.00 (0.0.0)</td><td>Initial public release</td></tr>
 * </table>*/
