/* Funky: a light-weight embeddable programming language
 * Copyright (c) 2007, Ronald Landheer-Cieslak
 * All rights reserved
 * 
 * This is free software. You may distribute it and/or modify it and
 * distribute modified forms provided that the following terms are met:
 *
 * * Redistributions of the source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer;
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution;
 * * None of the names of the authors of this software may be used to endorse
 *   or promote this software, derived software or any distribution of this 
 *   software or any distribution of which this software is part, without 
 *   prior written permission from the authors involved;
 * * Unless you have received a written statement from Ronald Landheer-Cieslak
 *   that says otherwise, the terms of the GNU General Public License, as 
 *   published by the Free Software Foundation, version 2 or (at your option)
 *   any later version, also apply.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/** \page building Building Funky
 * To build Funky, you will need a fairly recent compiler. It has been tested
 * (so far) with: 
 * \li g++ (GCC) 3.3.5 (Debian 1:3.3.5-13)
 * \li g++ (GCC) 4.1.2 (Ubuntu 1:4.1.2-0ubuntu4)
 * \li Microsoft Visual C++ 2005
 * \li g++ (GCC) 3.4.4 (cygming special, gdc 0.12, using dmd 0.125)
 *
 * \section building_gnu Building on a GNU system
 * \subsection prereqs_debian Prerequisites on a Debian/Ubuntu system
 * To build Funky, you will need the following installed on your system:
 * \li g++
 * \li libboost-dev
 * \li libloki-dev
 * \li libcppunit-dev (for the test cases)
 * \li autoconf
 * \li libtool
 * \li automake (for aclocal - not actually used)
 *
 * libloki-dev is not available on Debian Sarge. On those systems you will 
 * have to not download it from http://loki-lib.sf.net and build it yourself.
 *
 * \subsection prereqs_cygwin Prerequisites on a Cygwin system
 * On Cygwin, you will need:
 * \li boost-devel
 * \li gcc-g++
 * \li libtool1.5
 * \li autoconf2.5
 * \li automake (for aclocal)
 * 
 * Loki is not available on Cygwin, so you will have to download it from
 * http://loki-lib.sf.net and build it yourself.
 * 
 * CPPUnit \em is available from Cygwin, but I've had some trouble using it.
 * I recommend you download it from http://cppunit.sf.net and build it yourself.
 * 
 * \subsection building_gnu2 Building
 * Once you have all the prerequisites installed, do the following:
 * \code
 * tar xjvf funky-<version>.tar.bz2
 * cd funky-<version>
 * ./bootstrap
 * mkdir .build
 * cd .build
 * ../configure
 * make
 * make check # run the test cases
 * \endcode
 * The test cases take a lot less time than building Funky itself. None should
 * fail. If they do, you've found a bug that I haven't found on any of the above
 * platforms. \b Please report the bug to me! (but do read the FAQ first)
 * 
 * \section building_msvc8 Building with MSVC8
 * In order to build with MSVC8 (MS Visual Studio 2005) you need both Boost
 * (download from http://www.boost.org) and Loki (download from
 * http://loki-lib.sf.net) and, for the test cases CPPUnit (download from 
 * http://cppunit.sf.net).
 *
 * Once you've got those prerequisites built and installed somewhere where
 * MSVC8 will find them (the project files make no assumptions as to their
 * location) you can start building Funky.
 *
 * To do that, do the following:
 * \li unpack funky-<version>.tar.bz2\n
 *     Several tools will allow you to do this, including WinRAR
 *     (http://rarlab.com) which is what I use.
 * \li go to funky-<version>\\project\\msvc8 and open Funky.sln
 * \li build (Ctrl-Shift-B or F7, depending on your settings)
 *
 * As I have a few build scripts installed in my installation, you may have
 * to go look for the output files in a directory called c:\\cvstempo1
 * */
