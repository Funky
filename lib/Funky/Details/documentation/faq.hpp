/* Funky: a light-weight embeddable programming language
 * Copyright (c) 2007, Ronald Landheer-Cieslak
 * All rights reserved
 * 
 * This is free software. You may distribute it and/or modify it and
 * distribute modified forms provided that the following terms are met:
 *
 * * Redistributions of the source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer;
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution;
 * * None of the names of the authors of this software may be used to endorse
 *   or promote this software, derived software or any distribution of this 
 *   software or any distribution of which this software is part, without 
 *   prior written permission from the authors involved;
 * * Unless you have received a written statement from Ronald Landheer-Cieslak
 *   that says otherwise, the terms of the GNU General Public License, as 
 *   published by the Free Software Foundation, version 2 or (at your option)
 *   any later version, also apply.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/** \page faq Frequently Asked Questions
 * <dl>
 * <dt>Why does Funky take so long to compile?</dt>
 * <dd>Funky is implemented using Boost.Spirit. Spirit extensively uses 
 *     C++'s templates, and forces your compiler to work hard for a 
 *     living. The advantage is that the generated code is pretty close
 *     to optimal while the code in C++ is very close to BNF (as far as
 *     the grammar is concerned).</dd>
 * <dt>Why doesn't Funky have any support for named variables?</dt>
 * <dd>Funky doesn't have variables because it's a functional programming
 *     language and, as such, doesn't need any variables. It's a very 
 *     different rogramming "paradigm" that you may be used to, but it
 *     is a very powerul one once it's well-understood.</dd>
 * <dt>Why does Funky only have one built-in type?</dt>
 * <dd>Funky doesn't support different types because there has, at least
 *     until now, not been a real need for them. If support for different
 *     types is needed, support will be added. All the necessary preparations
 *     for such an addition have been made when Groovy was implemented, so
 *     adding support for a Funky variant with more than one built-in type
 *     should be a trivial affair.</dd>
 * <dt>Why doesn't Funky support higher-order functions?</dt>
 * <dd>Supporting higher-order functions for installable functions (i.e.
 *     C++ functions you can install in the interpreter) would imply
 *     exposing part of the implementation, which would be relatively
 *     trivial to do and will be done if there's a need for it.\n
 *     Supporting higher-order functions within the language's scripts
 *     (i.e. functions that take functions as arguments and/or return
 *     functions) would imply a relatively small change to the grammar,
 *     but would introduce potential ambiguity that would only be possible
 *     to resolve when the script is actually run. That is not really a
 *     blocking problem either, so it will be implemented when the need
 *     arises.\n
 *     If you really have a need for this, contact me so we can work out
 *     the details of what you need (but remember that a small fee may
 *     apply if you want me to make modifications to this work on-demand).
 *     </dd>
 * <dt>The test cases crahs with a Segmentation Fault/Access Violation
 *     without producing any output!</dt>
 * <dd>\b On \b Windows: if you've compiled with MSVC8, make sure you've
 *     compiled CPPUnit and Funky with the same settings. Most notably, 
 *     make sure that under Funky's project properties, under "C/C++",
 *     under "Code Generation" the setting for the "Runtime Library" 
 *     is the same for both (usually /MT%, /MTd, /MD or /MDd).\n
 *     \b On \b GNU (including Cygwin): make sure the CPPUnit library
 *     you're using was compiled with the same version of G++ as you're
 *     using to compile Funky, and linked with the same version of
 *     libstdc++. Not doing so will result in the compiled program using
 *     two different versions of \c std::string, which breaks the One
 *     Definition Rule and will crash the program.</dd>
 * </dl>
 */
