/* Funky: a light-weight embeddable programming language
 * Copyright (c) 2007, Ronald Landheer-Cieslak
 * All rights reserved
 * 
 * This is free software. You may distribute it and/or modify it and
 * distribute modified forms provided that the following terms are met:
 *
 * * Redistributions of the source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer;
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution;
 * * None of the names of the authors of this software may be used to endorse
 *   or promote this software, derived software or any distribution of this 
 *   software or any distribution of which this software is part, without 
 *   prior written permission from the authors involved;
 * * Unless you have received a written statement from Ronald Landheer-Cieslak
 *   that says otherwise, the terms of the GNU General Public License, as 
 *   published by the Free Software Foundation, version 2 or (at your option)
 *   any later version, also apply.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _funky_details_fetcher_h
#define _funky_details_fetcher_h

#include <boost/function.hpp>

namespace Funky
{
	namespace Details
	{
		template < typename ReturnType >
		class Fetcher;
		template < typename T >
		Fetcher< T > makeFetcher(T t);
		template < typename T >
		Fetcher< T > makeFetcher(boost::function0< T > t);

		template < typename ReturnType >
		class LiteralFetcher_
		{
		protected :
			LiteralFetcher_()
			{ /* no-op */ }

			LiteralFetcher_(ReturnType literal)
				: literal_(literal)
			{ /* no-op */ }

			ReturnType literal_;
		};

		template < typename ReturnType >
		class FunctionFetcher_ : protected LiteralFetcher_< ReturnType >
		{
		protected :
			typedef boost::function0< ReturnType > Function_;

			FunctionFetcher_()
			{ /* no-op */ }

			FunctionFetcher_( ReturnType t )
				: LiteralFetcher_(t)
			{ /* no-op */ }

			FunctionFetcher_( Function_ t )
			{ /* no-op */ }

			ReturnType perform_() const
			{
				if (!function_.empty())
					return function_();
				else
					return literal_;
			}

			Function_ function_;
		};

		template < typename ReturnType >
		class Fetcher : protected FunctionFetcher_< ReturnType > 
		{
		public :
			typedef int Fetcher< ReturnType >::* Bool_;

			ReturnType operator()() const
			{
				return perform_();
			}

			Fetcher()
			{ /* no-op */ }

			Fetcher( ReturnType t )
				: FunctionFetcher_(t)
			{ /* no-op */ }

			operator Bool_() const
			{
				return ((*this)()) ? &Fetcher< ReturnType >::used_for_cast_to_bool_ : 0;
			}

			operator ReturnType() const
			{
				return (*this)();
			}

		private :
			Fetcher( Function_ t )
				: FunctionFetcher_(t)
			{ /* no-op */ }

			int used_for_cast_to_bool_;

			friend Fetcher< ReturnType > makeFetcher<>(boost::function0< ReturnType > t);
		};

		template < typename T >
		Fetcher< T > makeFetcher(T t)
		{
			return Fetcher< T >(t);
		};

		template < typename T >
		Fetcher< T > makeFetcher(boost::function0< T > t)
		{
			return Fetcher< T >(t);
		};
	}
}

#endif
