/* Funky: a light-weight embeddable programming language
 * Copyright (c) 2007, Ronald Landheer-Cieslak
 * All rights reserved
 * 
 * This is free software. You may distribute it and/or modify it and
 * distribute modified forms provided that the following terms are met:
 *
 * * Redistributions of the source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer;
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution;
 * * None of the names of the authors of this software may be used to endorse
 *   or promote this software, derived software or any distribution of this 
 *   software or any distribution of which this software is part, without 
 *   prior written permission from the authors involved;
 * * Unless you have received a written statement from Ronald Landheer-Cieslak
 *   that says otherwise, the terms of the GNU General Public License, as 
 *   published by the Free Software Foundation, version 2 or (at your option)
 *   any later version, also apply.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/** \mainpage 
 * \section introduction The Funky embeddable interpreter.
 * Funky is a tiny, embeddable, functional programming language inspired 
 * from lisp. Its grammar is small and unambiguous, which makes it very 
 * easy to write a script in Funky, which you could embed in any program.
 *
 * \section grammar The Grammar
 * The grammar, in BNF, looks like this:
 * \code
 * literal                      = < in Funky, any real number, using a dot as decimal; in Groovy, any integer number >
 *                              ;
 * parameter                    = '@'[0-9]+ | "@@"
 *                              ;
 * function_name                = [A-Za-z][A-Za-z0-9]*
 *                              ;
 * comma_separated_list         = ( ',' ( statement | literal | parameter ) )*
 *                              ;
 * statement                    = '(' function_name comma_separated_list ')'
 *                              ;
 * function_definition          = '(' '!' function_name function_param_count? ',' statement ')'
 *                              ;
 * function_param_count         = '-' [0-9]+
 *                              ;
 * script                       = function_definition* statement
 *                              ;
 * \endcode
 *
 * As you can see, the grammar is pretty straight-forward. The current 
 * version of the language does not support some of the more extravagant
 * features of other Lisp-like languages, such as Lambda-expressions, but
 * if the need arises, such features are pretty easy to add to such a 
 * little language as Funky is.
 *
 * \section when When use Funky?
 * Funky was designed to be a very light-weight, embeddable scripting
 * language that's easy to extend (far) beyond its current contraints.
 * Note, though, that when I say "light-weight" I don't necessarilly mean 
 * that I went to extraordinary lengths to optimize Funky: the code is intended
 * first to be maintainable and have a certain elegance, and to be fast and
 * have a small footprint second.
 *
 * With it, you can add a scripting capacity to your own programs very easily:
 * just create an instance of the interpreter (Funky::Funky), install your 
 * own functions in it and have it evaluate any given script. Given the 
 * simplicity of the language, practically anyone with any typewriter skills 
 * can write scripts that your applications will automatically be able to 
 * run. All you need to do, if you want to be able to do any more than simple 
 * arithmetic, is install your functions so your applications' users can call them.
 *
 * So, use Funky when you need a light-weight, easy to use embedded scripting
 * language and you're writing your application in C++ (C bindings will 
 * be available if there's any demand for them).
 *
 * \section scripts What does a script look like?
 * \subsection operators Built-in operators
 * Funky comes with three build-in operators, all of which you can overload
 * as functions (but why would you want to do that?):
 * \li \c if evaluates its first argument and, if that results in something 
 *     non-zero, evaluates its second argument. Otherwise, it evaluates its
 *     third argument.
 *     Note that though all code is always parsed, the built-in support for \c if
 *     makes sure that the non-selected code really isn't evaluated. It is the
 *     \em only way Funky allows you to optionally evaluate scripted code!
 * \li \c defined, if passed a function call, checks whether the function exists
 *     and returns non-zero if so. If passed a parameter (or all parameters), it
 *     checks whether the parameter(s) exist and returns non-zero if so, zero if
 *     not.
 * \li \c shift returns the first argument (\@0) and pops it from the argument
 *     list.
 *
 * \subsection parameters Access to arguments
 * Funky allows you to pass arguments to built-in or user-defined functions. To
 * access any single argument, use the '@' sign and the zero-based index of the
 * argument, or use the "shift" operator. To access \em all arguments, say "@@".
 *
 * \subsection together Putting it all together
 * Here's a little script that uses all three built-in operators and all three
 * ways of accessing parameters:
 * \code
 * (!sum,
 * 	(if, (defined, @0),
 * 		(add,
 * 			(shift),
 * 			(if, (defined, @0), (sum, @@), 0)
 *		),
 * 		0
 * 	)
 * )
 * (sum, @@) 
 * \endcode
 * This script calculates the sum of all arguments passed to it, and you can
 * pass it any number of arguments. In C++, the equivalent code would be:
 * \code
 * double sum(const std::vector< double > & arguments)
 * {
 *	struct Accumulator
 *	{
 *		Accumulator()
 *			: value_(0)
 *		{ }
 *
 *		Accumulator & operator()(double d)
 *		{
 *			value_ += d;
 * 			return *this
 *		}
 *
 *		double value_;
 *	};
 *	return std::for_each(arguments.begin(), arguments.end(), Accumulator()).value_;
 * }
 * \endcode
 * (After all, we can do functional programming in C++). The major difference
 * here is that a functor is called for each value among the arguments, from first
 * to last whereas the Funky version recursively calls \c sum from the last argument
 * to the first. This is called "tail recursion" and \em could be optimized using
 * a loop, like the C++ version does, but the Funky interpreter does no optimization
 * whatsoever.
 *
 * Just for the heck of it, this is the \em real equivalent in C++:
 * \code
 * double sum(const std::vector< double > & arguments)
 * {
 *	if (arguments.size())
 *		return arguments[0] + sum(std::vector< double >(arguments.begin() + 1, arguments.end()));
 *	else return 0;
 * }
 * \endcode
 *
 * \section embedding Embedding the Funky interpreter
 * The Funky interpreter is designed to be easily embeddable in any program written 
 * in C++ (though it should be a rather trivial affair to write front-ends for other
 * languages, such as C#, C, Java or (Visual) Basic). It currently comes with two 
 * front-ends: \link Funky::Funky Funky \endlink and \link Funky::Groovy Groovy \endlink. 
 * The former is the language implementation using double-precision floating-points as
 * built-in type, while the latter uses integers as built-in types. To embed the interpreter, 
 * all you have to do is create an instance of the interpreter and install your functions 
 * in it. 
 *
 * \subsection extending Extending the Funky interpreter
 * In order to add support for an interpreter with a different built-in type than integer
 * or doubleprevision floating point, there are only two things you need to do: create
 * a literal parser for the type you want to parse, and write a front-end with that
 * type. The front-end itself is very straight-forward (you can look at the code of the
 * \link Funky::Funky Funky \endlink and \link Funky::Groovy Groovy \endlink classes 
 * to find out just how straight-forward they are) so creating a new front-end should
 * take you no more than a few minutes.
 *
 * \author Ronald Landheer-Cieslak <ronald@landheer-cieslak.com>
 */

/** The library's main namespace. Everything goes in here */
namespace Funky
{ 
	/** All exceptions thrown by the library are defined in this namespace. */
	namespace Exceptions
	{ /* here for documentation purposes only */ }
	/** Implementation details client code doesn't need to know about */
	namespace Private
	{ /* here for documentation purposes only */ }
}
