# the library
Funky_SRC = Funky.cpp Groovy.cpp Exceptions/ParseError.cpp

Funky_INSTALL_HEADERS += Details/prologue.h		\
                         Funky.h			\
			 Exceptions/ParseError.h	\
			 Exceptions/FunctionCallError.h

SRC += $(patsubst %,lib/Funky/%,$(Funky_SRC))

INSTALL_HEADERS += $(patsubst %,Funky/%,$(Funky_INSTALL_HEADERS))
Funky_OBJ := $(patsubst %.cpp,lib/Funky/%.lo,$(Funky_SRC))
OBJ += $(Funky_OBJ)

$(eval $(call LINK_LIBRARY_template,libFunky.la,$(Funky_OBJ)))
