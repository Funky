/* Funky: a light-weight embeddable programming language
 * Copyright (c) 2007, Ronald Landheer-Cieslak
 * All rights reserved
 * 
 * This is free software. You may distribute it and/or modify it and
 * distribute modified forms provided that the following terms are met:
 *
 * * Redistributions of the source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer;
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution;
 * * None of the names of the authors of this software may be used to endorse
 *   or promote this software, derived software or any distribution of this 
 *   software or any distribution of which this software is part, without 
 *   prior written permission from the authors involved;
 * * Unless you have received a written statement from Ronald Landheer-Cieslak
 *   that says otherwise, the terms of the GNU General Public License, as 
 *   published by the Free Software Foundation, version 2 or (at your option)
 *   any later version, also apply.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _funky_exceptions_functioncallerror_h
#define _funky_exceptions_functioncallerror_h

namespace Funky
{
	namespace Exceptions
	{
		/** Thrown when the evaluator finds something wrong with a function call.
		 * There are a few errors you might be able to make in your scripts. 
		 * Most involve calling a function with the wrong amount of arguments
		 * or a function returning the wrong amount of values. These errors are
		 * diagnosed by the evaluator when a parsed script is run. Note that they
		 * cannot be diagnosed when the script is first parsed, as the necessary 
		 * functions may either not have been installed in the interpreter yet, or 
		 * may be overridden by either the script or the client application. */
		struct FunctionCallError : public std::runtime_error
		{
			//! The type of he reason of the error
			enum Reason
			{
				//! A function was called with the wrong amount of arguments
				argument_count_mismatch__,
				//! An unknown function was called
				unknown_function__,
				//! Shift was called while there were no more arguments left
				no_more_arguments__,
				//! An argument was dereferenced with the @ sign, but the reference referred to an argument that does not exist
				argument_out_of_range__,
				//! A statement was found where it shouldn't have been (e.g. as argument to \c defined)
				unexpected_statement__,
				//! A function returned the wrong amount of values
				return_value_count_mismatch__
			};

			/** Construct an instance of the exception class.
			 * \param reason the reason the exception is thrown
			 * \param expected the value that was expected in stead of what we got
			 * \param got the value we got in stead of what we expected */
			FunctionCallError(Reason reason, int expected = 0, int got = 0)
				: std::runtime_error("Function call error"),
				  reason_(reason),
				  expected_(expected),
				  got_(got)
			{ /* no-op */ }

			//! The reason the exception was thrown
			Reason reason_;
			//! the value that was expected in stead of what we got
			int expected_;
			//! the value we got in stead of what we expected */
			int got_;
		};
	}
}

#endif
