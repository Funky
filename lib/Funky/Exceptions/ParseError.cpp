/* Funky: a light-weight embeddable programming language
 * Copyright (c) 2007, Ronald Landheer-Cieslak
 * All rights reserved
 * 
 * This is free software. You may distribute it and/or modify it and
 * distribute modified forms provided that the following terms are met:
 *
 * * Redistributions of the source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer;
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution;
 * * None of the names of the authors of this software may be used to endorse
 *   or promote this software, derived software or any distribution of this 
 *   software or any distribution of which this software is part, without 
 *   prior written permission from the authors involved;
 * * Unless you have received a written statement from Ronald Landheer-Cieslak
 *   that says otherwise, the terms of the GNU General Public License, as 
 *   published by the Free Software Foundation, version 2 or (at your option)
 *   any later version, also apply.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "ParseError.h"
#include <algorithm>
#include <cassert>
#include <boost/format.hpp>

void STR_DUP(char *& target, const char * source)					
{												
	const char * s(source);					
	const char * end = s;						
	while (*end != 0) ++end;					
	target = new char[(end - s) + 1];			
	std::copy(s, end, target);					
	target[end - s] = 0;						
}

namespace Funky
{
	namespace Exceptions
	{
		ParseError::ParseError(const ParseError & e)
			: std::runtime_error(e),
			  reason_(e.reason_),
			  offset_(e.offset_),
			  what_(e.what_)
		{
			e.what_ = 0;
		}
			
		ParseError::~ParseError() throw()
		{
			delete[] what_;
		}

		ParseError & ParseError::swap(ParseError & e)
		{
			std::swap(reason_, e.reason_);
			std::swap(offset_, e.offset_);
			std::swap(what_, e.what_);

			return *this;
		}

		const char * ParseError::what() const throw()
		{
			if (!what_)
			{
				try
				{
					boost::format msg("Parse error: \"%1%\" at offset %2%");
					switch (reason_)
					{
					case incomplete_parse__ :
						msg % "Failed to completely parse input";
						break;
					case function_name_expected__ :
						msg % "Function name expected";
						break;
					case left_parenthesis_expected__ :
						msg % "Left parenthesis expected";
						break;
					case right_parenthesis_expected__ :
						msg % "Right parenthesis expected";
						break;
					case literal_parameter_or_statement_expected__ :
						msg % "Literal, parameter or statement expected";
						break;
					default :
						msg % "Unknown error";
					}
					msg % offset_;
					STR_DUP(what_, msg.str().c_str());
				}
				catch (...)
				{
					return "Parse error";
				}
			}
			else
			{ /* already have a what_ */ }
			assert(what_);
			return what_;
		}
	}
}

