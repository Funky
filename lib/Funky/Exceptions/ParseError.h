/* Funky: a light-weight embeddable programming language
 * Copyright (c) 2007, Ronald Landheer-Cieslak
 * All rights reserved
 * 
 * This is free software. You may distribute it and/or modify it and
 * distribute modified forms provided that the following terms are met:
 *
 * * Redistributions of the source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer;
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution;
 * * None of the names of the authors of this software may be used to endorse
 *   or promote this software, derived software or any distribution of this 
 *   software or any distribution of which this software is part, without 
 *   prior written permission from the authors involved;
 * * Unless you have received a written statement from Ronald Landheer-Cieslak
 *   that says otherwise, the terms of the GNU General Public License, as 
 *   published by the Free Software Foundation, version 2 or (at your option)
 *   any later version, also apply.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _funky_exceptions_parseerror_h
#define _funky_exceptions_parseerror_h

#include <stdexcept>

namespace Funky
{
	namespace Exceptions
	{
		/** Thrown when the parser finds a non-recoverable error.
		 * The Funky grammar is pretty unambiguous, so the parser can diagnose
		 * quite a few problems (for such a small language). The parser will 
		 * throw an instance of this class if it finds any of the errors it 
		 * can diagnose, or if it doesn't succeed in completely parsing its
		 * input. */
		struct ParseError : public std::runtime_error
		{
			//! The type of the reason for the exception
			enum Reason
			{
				//! Failed to completely parse input
				incomplete_parse__,
				//! Function name expected
				function_name_expected__,
				//! Left parenthesis expected
				left_parenthesis_expected__,
				//! Right parenthesis expected
				right_parenthesis_expected__,
				//! Literal, parameter or statement expected
				literal_parameter_or_statement_expected__,
			};

			/** Construct an instance of the exception.
			 * \param reason the reason of the error
			 * \param offset the offset in the input where the error occured */
			ParseError(Reason reason, unsigned int offset)
				: std::runtime_error("Parse error"),
				  reason_(reason),
				  offset_(offset),
				  what_(0)
			{ /* no-op */ }

			ParseError(const ParseError & e);
			~ParseError() throw();
			ParseError & operator=(ParseError e)
			{ return swap(e); }
			ParseError & swap(ParseError & e);

			//! Get a human-readible explanation of the error
			const char * what() const throw();

			//! Code containing the reason of the error
			Reason reason_;
			//! Offset of the error in the input
			unsigned int offset_;

		private :
			mutable char * what_;
		};
	}
}

#endif
