/* Funky: a light-weight embeddable programming language
 * Copyright (c) 2007, Ronald Landheer-Cieslak
 * All rights reserved
 * 
 * This is free software. You may distribute it and/or modify it and
 * distribute modified forms provided that the following terms are met:
 *
 * * Redistributions of the source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer;
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution;
 * * None of the names of the authors of this software may be used to endorse
 *   or promote this software, derived software or any distribution of this 
 *   software or any distribution of which this software is part, without 
 *   prior written permission from the authors involved;
 * * Unless you have received a written statement from Ronald Landheer-Cieslak
 *   that says otherwise, the terms of the GNU General Public License, as 
 *   published by the Free Software Foundation, version 2 or (at your option)
 *   any later version, also apply.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _funky_private_evaluator_h
#define _funky_private_evaluator_h

#include "StoredFunctions.h"
#include "../Details/Fetcher.h"

namespace Funky
{
	namespace Private
	{
		template < typename T >
		struct get_lexical_cast_target_type
		{
			typedef T type;
		};

		template < typename T >
		struct get_lexical_cast_target_type< Details::Fetcher< T > >
		{
			typedef T type;
		};

		template < typename Arguments >
		struct Evaluator
		{
			typedef boost::spirit::tree_match< const char * >::tree_iterator TreeIterator; // This is the type of an iterator with which we can traverse the parse tree
			typedef boost::spirit::tree_node< boost::spirit::node_val_data<> > Node; // This is the type of a node in the parse tree
			typedef std::stack< std::pair< Node, Arguments * > > Stack;

			Evaluator(const StoredFunctions< Arguments > & functions)
				: functions_(functions),
				  value_(0)
			{ /* no-op */ }

			Evaluator(const StoredFunctions< Arguments > & functions, const boost::any & user)
				: functions_(functions),
				  value_(0),
				  user_(user)
			{ /* no-op */ }

			Evaluator & operator()(const Node & node, Arguments * arguments)
			{
				stack_.push(std::make_pair(node, arguments));
				Loki::ScopeGuard stack_guard(Loki::MakeObjGuard(stack_, &Stack::pop));

				Arguments args; // these are the ones we will pass to the next function we call

				if (stack_.top().first.value.id() == function_name_id__)
				{
					std::string function_to_call(stack_.top().first.value.begin(), stack_.top().first.value.end());
					// look up the function
					typename StoredFunctions< Arguments >::const_iterator which(functions_.find(function_to_call));
					if (which == functions_.end() && function_to_call != "shift")
						throw Exceptions::FunctionCallError(Exceptions::FunctionCallError::unknown_function__);
					else if (function_to_call == "shift")
					{
						if (stack_.top().second->empty())
							throw Exceptions::FunctionCallError(Exceptions::FunctionCallError::no_more_arguments__);
						else
						{
							value_ = Arguments(1, stack_.top().second->at(0));
							stack_.top().second->erase(stack_.top().second->begin());
						}
					}
					else
					{
						Arguments no_args;
						value_ = which->second->call(*this, &no_args);
					}
				}
				else
				{ // function call with arguments
					TreeIterator curr(stack_.top().first.children.begin());

					/* normally, the first node we find is always the name of a 
					 * function to call. After that, we will find any number of 
					 * arguments to pass to it, but those arguments can be either 
					 * literals, parameters passed to us, or statements. If they 
					 * are statements, we will need to evaluate them. If they are
					 * literals, we have the value we want; if they are parameters,
					 * they are in the stack's current top. */
					// get the name of the next function to call
					assert(curr->value.id() == function_name_id__);
					std::string function_name(curr->value.begin(), curr->value.end());
					typename StoredFunctions< Arguments >::const_iterator which(functions_.find(function_name));
					enum { custom__, if__, defined__ } function_type(custom__);
					if (which == functions_.end())
					{
						/* three possibilities: either it's an "if", for which we 
						 * have a built-in version right here; or it's a "defined", 
						 * in which case there should be one argument, which should be
						 * a parameter, or it's an unknown function, in which case we
						 * throw an exception.
						 * Note that we only start checking whether it's an "if" or 
						 * a "defined" *after" we haven't found the function in the 
						 * function map: we leave the possibility to the user to
						 * override both built-in operators, just like we leave
						 * him the possibility to override any built-in function. 
						 * There is no such thing as a reserved word in Funky. */
						if (function_name == "if")
							function_type = if__;
						else if (function_name == "defined")
							function_type = defined__;
						else
							throw Exceptions::FunctionCallError(Exceptions::FunctionCallError::unknown_function__);
					}
					else
					{ /* we know this function - now call it */ }
					++curr;
					assert(curr->value.id() == comma_separated_list_id__);
					TreeIterator end(curr->children.end());
					TreeIterator curr_arg(curr->children.begin());
					unsigned int argument_index(0);
					bool evaluate_second(true);
					bool evaluate_third(true);
					if (function_type == if__ && std::distance(curr_arg, end) != 6 /* counting commas */)
						throw Exceptions::FunctionCallError(Exceptions::FunctionCallError::argument_count_mismatch__, 3, std::distance(curr_arg, end) / 2);
					else
					{ /* all is well so far */ }
					if (function_type == defined__ && std::distance(curr_arg, end) != 2 /* counting commas */)
						throw Exceptions::FunctionCallError(Exceptions::FunctionCallError::argument_count_mismatch__, 1, std::distance(curr_arg, end) / 2);
					else
					{ /* all is well so far */ }
					do
					{
						assert(curr_arg == end || *(curr_arg->value.begin()) == ',');
						if (curr_arg != end)
						{
							++curr_arg;
							assert(curr_arg != end);
							if ((argument_index == 1 && !evaluate_second) ||
								(argument_index == 2 && !evaluate_third))
							{ /* skip this argument */ }
							else
							{
								switch (curr_arg->value.id().to_long())
								{
								case literal_id__ :
									if (function_type == defined__)
										args.push_back(1); // literals are always defined
									else
										args.push_back(boost::lexical_cast< typename get_lexical_cast_target_type< typename Arguments::value_type>::type >(std::string(curr_arg->value.begin(), curr_arg->value.end())));
									break;
								case parameter_id__ :
									{
										std::string parameter_id(std::string(curr_arg->value.begin(), curr_arg->value.end()));
										assert(parameter_id.size() > 1);
										if (parameter_id != "@@")
										{
											unsigned int index(boost::lexical_cast<unsigned int>(parameter_id.substr(1)));
											if (stack_.top().second->size() <= index && function_type != defined__)
												throw Exceptions::FunctionCallError(Exceptions::FunctionCallError::argument_out_of_range__, stack_.top().second->size() - 1, index);
											else if (function_type == defined__ && stack_.top().second->size() <= index)
												args.push_back(0);
											else if (function_type == defined__)
												args.push_back(1);
											else
												args.push_back(stack_.top().second->at(index));
										}
										else
										{
											if (function_type == defined__)
												args.push_back(!stack_.top().second->empty());
											else // copy all arguments
												std::copy(stack_.top().second->begin(), stack_.top().second->end(), std::back_inserter(args));
										}
									}
									break;
								case statement_id__ :
									{
										// can't pass a statement to defined
										if (function_type == defined__)
											throw Exceptions::FunctionCallError(Exceptions::FunctionCallError::unexpected_statement__);
										else
										{ /* not a defined call */ }
										Arguments temp_args((*this)(*curr_arg, stack_.top().second).value_);
										std::copy(temp_args.begin(), temp_args.end(), std::back_inserter(args));
									}
									break;
								case function_name_id__ : // a function call without arguments
									// can't pass a function call to defined
									if (function_type == defined__)
										throw Exceptions::FunctionCallError(Exceptions::FunctionCallError::unexpected_statement__);
									else
									{ /* not a defined call */ }
									{
										std::string function_to_call(curr_arg->value.begin(), curr_arg->value.end());
										// look up the function
										typename StoredFunctions< Arguments >::const_iterator which(functions_.find(function_to_call));
										if (which == functions_.end() && function_to_call != "shift")
											throw Exceptions::FunctionCallError(Exceptions::FunctionCallError::unknown_function__);
										else if (function_to_call == "shift")
										{
											if (stack_.top().second->empty())
												throw Exceptions::FunctionCallError(Exceptions::FunctionCallError::no_more_arguments__);
											else
											{
												args.push_back(stack_.top().second->at(0));
												stack_.top().second->erase(stack_.top().second->begin());
											}
										}
										else
										{
											Arguments no_args;
											Arguments returned_args(which->second->call(*this, &no_args));
											std::copy(returned_args.begin(), returned_args.end(), std::back_inserter(args));
										}
									}
									break;
								default :
									throw std::logic_error("Unexpected node type");
								}
							}
							++curr_arg;
							++argument_index;
							if (function_type == if__ && argument_index == 1)
							{
								// this is where we make the choice whether or not to evaluate the next, second argument
								if (!args.back())
									evaluate_second = false;
								else
									evaluate_third = false;
							}
						}
						else
						{ /* at the end */ }
					} while (curr_arg != end);
					if (function_type != if__ && function_type != defined__)
						value_ = which->second->call(*this, &args);
					else
						value_ = Arguments(1, args.back());
				}
				return *this;
			}

			const StoredFunctions< Arguments > & functions_;
			Stack stack_;
			Arguments value_;
			boost::any user_;
		};
	}
}

#endif

