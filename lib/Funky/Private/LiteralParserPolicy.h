#ifndef _funky_private_literalparserpolicy_h
#define _funky_private_literalparserpolicy_h

#include "ParserIDs.h"

namespace Funky
{
	namespace Private
	{
		template < typename LiteralType >
		struct LiteralParserPolicy;

		template <>
		struct LiteralParserPolicy< double >
		{
			template < typename ScannerT >
			struct definition
			{
				definition()
				{
					using namespace boost::spirit;
					literal_	= leaf_node_d[lexeme_d[real_p]]
								;
				};
				boost::spirit::rule< ScannerT, boost::spirit::parser_context<>, boost::spirit::parser_tag< literal_id__ > > literal_;
			};
		};

		template <>
		struct LiteralParserPolicy< int >
		{
			template < typename ScannerT >
			struct definition
			{
				definition()
				{
					using namespace boost::spirit;
					literal_	= leaf_node_d[lexeme_d[int_p]]
								;
				};
				boost::spirit::rule< ScannerT, boost::spirit::parser_context<>, boost::spirit::parser_tag< literal_id__ > > literal_;
			};
		};
	}
}

#endif
