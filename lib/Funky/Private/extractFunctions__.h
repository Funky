/* Funky: a light-weight embeddable programming language
 * Copyright (c) 2007, Ronald Landheer-Cieslak
 * All rights reserved
 * 
 * This is free software. You may distribute it and/or modify it and
 * distribute modified forms provided that the following terms are met:
 *
 * * Redistributions of the source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer;
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution;
 * * None of the names of the authors of this software may be used to endorse
 *   or promote this software, derived software or any distribution of this 
 *   software or any distribution of which this software is part, without
 *   prior written permission from the authors involved;
 * * Unless you have received a written statement from Ronald Landheer-Cieslak
 *   that says otherwise, the terms of the GNU General Public License, as 
 *   published by the Free Software Foundation, version 2 or (at your option)
 *   any later version, also apply.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
namespace Funky
{
	namespace Private
	{
		template < typename Arguments >
		void extractFunctions__(
			const boost::spirit::tree_parse_info<> & parse_result,
			StoredFunctions< Arguments > & functions,
			boost::spirit::tree_node< boost::spirit::node_val_data<> > & main)
		{
			// This is the type of an iterator with which we can traverse the parse tree
			typedef boost::spirit::tree_match< const char * >::const_tree_iterator TreeIterator;

			TreeIterator root_node(parse_result.trees.begin());
			assert(root_node != parse_result.trees.end());
			// we only have a script_id__ if there is at least one function before the first statement. Otherwise, the root node is a statement_id__
			if (root_node->value.id() == script_id__)
			{
				// for each function definition,
				for (TreeIterator current_function(root_node->children.begin()); current_function != root_node->children.end(); ++current_function)
				{
					if (current_function->value.id() == function_definition_id__)
					{
						TreeIterator function_definition_node(current_function->children.begin());
						assert(
							std::distance(function_definition_node, current_function->children.end()) == 4 || 
							std::distance(function_definition_node, current_function->children.end()) == 5);
						assert(function_definition_node->value.id() == function_definition_token_id__);
						++function_definition_node;

						// extract the name
						assert(function_definition_node->value.id() == function_name_id__);
						std::string function_name(function_definition_node->value.begin(), function_definition_node->value.end());
						++function_definition_node;
						// extract the number of expected arguments, if known
						int expected_parameter_count(-1);
						if (function_definition_node->value.id() == function_param_count_id__)
						{
							expected_parameter_count = boost::lexical_cast< int >(
								std::string(function_definition_node->value.begin(), function_definition_node->value.end()));
							++function_definition_node;
						}
						else
						{ /* unknown parameter count */ }

						++function_definition_node; // (the colon)
						// extract the statement
						// store the statement in our function map, as a tree
						boost::shared_ptr< ParsedFunction< Arguments > > function(new ParsedFunction< Arguments >(expected_parameter_count, *function_definition_node));
						typename StoredFunctions< Arguments >::iterator where(functions.find(function_name));
						if (where != functions.end())
							functions.erase(where);
						else
						{ /* not a duplicate definition */ }
						functions.insert(typename StoredFunctions< Arguments >::value_type(function_name, function));
					}
					else
					{
						assert(current_function->value.id() == statement_id__);
						main = *current_function;
					}
				}
			}
			else
			{
				assert(
					root_node->value.id() == statement_id__ ||
					root_node->value.id() == function_name_id__ /* call without arguments */);
				main = *root_node;
			}
		}
	}
}
