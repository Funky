#ifndef _funky_private_parserids_h
#define _funky_private_parserids_h

namespace Funky
{
	namespace Private
	{
		/* Some rules have special identifiers, with which we can recognize them when we walk through the parse tree */
		enum
		{
			script_id__ = 1,
			statement_id__,
			function_definition_id__,
			comma_separated_list_id__,
			function_name_id__,
			parameter_id__,
			literal_id__,
			function_definition_token_id__,
			function_param_count_id__
		};
	}
}

#endif
