/* Funky: a light-weight embeddable programming language
 * Copyright (c) 2007, Ronald Landheer-Cieslak
 * All rights reserved
 * 
 * This is free software. You may distribute it and/or modify it and
 * distribute modified forms provided that the following terms are met:
 *
 * * Redistributions of the source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer;
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution;
 * * None of the names of the authors of this software may be used to endorse
 *   or promote this software, derived software or any distribution of this 
 *   software or any distribution of which this software is part, without 
 *   prior written permission from the authors involved;
 * * Unless you have received a written statement from Ronald Landheer-Cieslak
 *   that says otherwise, the terms of the GNU General Public License, as 
 *   published by the Free Software Foundation, version 2 or (at your option)
 *   any later version, also apply.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _funky_private_grammar_h
#define _funky_private_grammar_h

#include <boost/function.hpp>
#include <boost/bind.hpp>
#include "LiteralParserPolicy.h"
#include "ParserIDs.h"

namespace Funky
{
	namespace Private
	{
		// This implements the grammar to be used by the parser
		template < typename LiteralType >
		struct Grammar : public boost::spirit::grammar< Grammar< LiteralType > >
		{
			typedef LiteralParserPolicy< LiteralType > LiteralParserPolicy_;

			template <typename ScannerT>
			struct definition
			{
				definition(Grammar const& self)
					: expect_function_name_(Exceptions::ParseError::function_name_expected__),
					  expect_left_parenthesis_(Exceptions::ParseError::left_parenthesis_expected__),
					  expect_right_parenthesis_(Exceptions::ParseError::right_parenthesis_expected__),
					  expect_literal_parameter_or_statement_(Exceptions::ParseError::literal_parameter_or_statement_expected__)
				{
					using namespace boost::spirit;

					literal_					= literal_parser_policy_.literal_
												;
					parameter_					= leaf_node_d[lexeme_d[ ch_p('@') >> (+digit_p | ch_p('@'))]]
												;
					function_name_				= leaf_node_d[lexeme_d[alpha_p >> *( alnum_p )]]
												;
					comma_separated_list_		= *( ch_p(',') >> expect_literal_parameter_or_statement_( literal_ | parameter_ | statement_ ) )
												;
					statement_					= inner_node_d[ch_p('(') >> expect_function_name_(function_name_) >> comma_separated_list_  >> expect_right_parenthesis_(ch_p(')'))]
												;
					function_definition_		= inner_node_d[expect_left_parenthesis_(ch_p('(')) >> function_definition_token_ >> expect_function_name_(function_name_) >> !function_param_count_ >> ch_p(',') >> statement_ >> expect_right_parenthesis_(ch_p(')'))]
												;
					function_definition_token_	= ch_p('!')
												;
					function_param_count_		= ch_p('-') >> root_node_d[lexeme_d[ uint_p ]]
												;
					script_						= root_node_d[*function_definition_ >> statement_]
												;
				}

				boost::spirit::rule< ScannerT, boost::spirit::parser_context<>, boost::spirit::parser_tag< script_id__ > > const& start() const { return script_; }

				boost::spirit::rule< ScannerT, boost::spirit::parser_context<>, boost::spirit::parser_tag< script_id__ > > script_;
				boost::spirit::rule< ScannerT, boost::spirit::parser_context<>, boost::spirit::parser_tag< statement_id__ > > statement_;
				boost::spirit::rule< ScannerT, boost::spirit::parser_context<>, boost::spirit::parser_tag< function_definition_id__ > > function_definition_;
				boost::spirit::rule< ScannerT, boost::spirit::parser_context<>, boost::spirit::parser_tag< comma_separated_list_id__ > > comma_separated_list_;
				boost::spirit::rule< ScannerT, boost::spirit::parser_context<>, boost::spirit::parser_tag< function_name_id__ > > function_name_;
				boost::spirit::rule< ScannerT, boost::spirit::parser_context<>, boost::spirit::parser_tag< parameter_id__ > > parameter_;
				boost::spirit::rule< ScannerT, boost::spirit::parser_context<>, boost::spirit::parser_tag< literal_id__ > > literal_;
				boost::spirit::rule< ScannerT, boost::spirit::parser_context<>, boost::spirit::parser_tag< function_definition_token_id__ > > function_definition_token_;
				boost::spirit::rule< ScannerT, boost::spirit::parser_context<>, boost::spirit::parser_tag< function_param_count_id__ > > function_param_count_;

				typename Grammar::LiteralParserPolicy_::template definition< ScannerT > literal_parser_policy_;

				boost::spirit::assertion< Exceptions::ParseError::Reason > expect_function_name_;
				boost::spirit::assertion< Exceptions::ParseError::Reason > expect_left_parenthesis_;
				boost::spirit::assertion< Exceptions::ParseError::Reason > expect_right_parenthesis_;
				boost::spirit::assertion< Exceptions::ParseError::Reason > expect_literal_parameter_or_statement_;
			};
		};
	}
}

#endif
