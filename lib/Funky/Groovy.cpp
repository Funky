/* Funky: a light-weight embeddable programming language
 * Copyright (c) 2007, Ronald Landheer-Cieslak
 * All rights reserved
 * 
 * This is free software. You may distribute it and/or modify it and
 * distribute modified forms provided that the following terms are met:
 *
 * * Redistributions of the source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer;
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution;
 * * None of the names of the authors of this software may be used to endorse
 *   or promote this software, derived software or any distribution of this 
 *   software or any distribution of which this software is part, without
 *   prior written permission from the authors involved;
 * * Unless you have received a written statement from Ronald Landheer-Cieslak
 *   that says otherwise, the terms of the GNU General Public License, as 
 *   published by the Free Software Foundation, version 2 or (at your option)
 *   any later version, also apply.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "Groovy.h"
#include <boost/spirit.hpp>
#include <boost/spirit/tree/ast.hpp>
#include <boost/format.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>
#include <loki/ScopeGuard.h>
#include <stack>
#include <map>
#include "Exceptions/FunctionCallError.h"
#include "Exceptions/ParseError.h"
#include "Private/StoredFunction.h"
#include "Private/StoredFunctions.h"
#include "Private/Grammar.h"
#include "Private/Evaluator.h"
#include "Private/FunctionPointer.h"
#include "Private/FunctionPointer2.h"
#include "Private/ParsedFunction.h"
#include "Private/Language.h"
#include "Private/extractFunctions__.h"

namespace Funky
{
	using namespace Private;

	struct Groovy::Data
	{
		// This is the type of a node in the parse tree
		typedef boost::spirit::tree_node< boost::spirit::node_val_data<> > Node;

		// the value to return to the caller, from run() or eval()
		int retval_;
		// the parameters passed to the script
		Arguments parameters_;

		// the functions installed by either the user or the parser. This is the equivalent of a symbol table
		StoredFunctions< Arguments > functions_;
		// the main function - the last statement in the script
		Node main_;
	};

	Groovy::Groovy()
		: data_(new Data),
		  grammar_(new Grammar< int >())
	{
		// install the built-in functions
		installFunction("test", Function(boost::bind(&Language::test__< int >, _1)), 1);
		installFunction("or",	Function(boost::bind(&Language::or__< int >, _1)), 2);
		installFunction("and",	Function(boost::bind(&Language::and__< int >, _1)), 2);
		installFunction("not",	Function(boost::bind(&Language::not__< int >, _1)), 1);
		installFunction("add",	Function(boost::bind(&Language::add__< int >, _1)), 2);
		installFunction("neg",	Function(boost::bind(&Language::neg__< int >, _1)), 1);
		installFunction("mul",	Function(boost::bind(&Language::mul__< int >, _1)), 2);
		installFunction("div",	Function(boost::bind(&Language::div__< int >, _1)), 2);
	}

	Groovy::~Groovy()
	{
		delete grammar_;
		delete data_;
	}

	void Groovy::parse(const std::string & program_text)
	{
		boost::spirit::tree_parse_info<> parse_result;

		try
		{
			parse_result = boost::spirit::ast_parse(program_text.c_str(), *grammar_, boost::spirit::space_p);
		}
		catch (const boost::spirit::parser_error< Exceptions::ParseError::Reason > & e)
		{
			throw Exceptions::ParseError(e.descriptor, static_cast< unsigned int >(e.where - program_text.c_str()));
		}
		if (!parse_result.full)
		{
			throw Exceptions::ParseError(Exceptions::ParseError::incomplete_parse__, parse_result.length);
		}
		else
		{ /* all is well so far */ }

		/* Once the text is parsed, we have a parse tree. We now need to extract,
		 * from that tree, any functions we may have found in order to put them in
		 * the symbol table (data_->functions_) and the last statement, which is 
		 * the one that will be run by run(), which we put in data_->main_. */
		extractFunctions__(parse_result, data_->functions_, data_->main_);
	}

	int Groovy::run(const Arguments & _arguments/* = Arguments()*/) const
	{
		Arguments arguments(_arguments);
		Evaluator< Arguments > evaluator( data_->functions_ );
		evaluator(data_->main_, &arguments);
		if (evaluator.value_.size() != 1)
			throw Exceptions::FunctionCallError(Exceptions::FunctionCallError::return_value_count_mismatch__, 1, static_cast< int >(evaluator.value_.size()));
		else
		{ /* all is well */ }
		return evaluator.value_.front();
	}

	int Groovy::run(const boost::any & user, const Arguments & _arguments/* = Arguments()*/)
	{
		Arguments arguments(_arguments);
		Evaluator< Arguments > evaluator( data_->functions_, user );
		evaluator(data_->main_, &arguments);
		if (evaluator.value_.size() != 1)
			throw Exceptions::FunctionCallError(Exceptions::FunctionCallError::return_value_count_mismatch__, 1, static_cast< int >(evaluator.value_.size()));
		else
		{ /* all is well */ }
		return evaluator.value_.front();
	}

	int Groovy::eval(const std::string & program_text, const Arguments & arguments)
	{
		parse(program_text);
		return run(arguments);
	}

	int Groovy::eval(const std::string & program_text, const boost::any & user, const Arguments & arguments)
	{
		parse(program_text);
		return run(user, arguments);
	}

	void Groovy::installFunction(const std::string & function_name, const Function & function, int expected_args/* = any__*/)
	{
		StoredFunctions< Arguments >::iterator where(data_->functions_.find(function_name));
		if (where != data_->functions_.end())
			data_->functions_.erase(where);
		else
		{ /* not a duplicate definition */ }

		data_->functions_.insert(StoredFunctions< Arguments >::value_type(function_name, boost::shared_ptr< StoredFunction< Arguments > >(new FunctionPointer< Arguments >(function, expected_args))));
	}

	void Groovy::installFunction(const std::string & function_name, const MultiReturnFunction & function, int expected_args/* = any__*/, int expected_returns/* = any__*/)
	{
		StoredFunctions< Arguments >::iterator where(data_->functions_.find(function_name));
		if (where != data_->functions_.end())
			data_->functions_.erase(where);
		else
		{ /* not a duplicate definition */ }

		data_->functions_.insert(StoredFunctions< Arguments >::value_type(function_name, boost::shared_ptr< StoredFunction< Arguments > >(new FunctionPointer< Arguments >(function, expected_args, expected_returns))));
	}

	void Groovy::installFunction2(const std::string & function_name, const Function2 & function, int expected_args/* = any__*/)
	{
		StoredFunctions< Arguments >::iterator where(data_->functions_.find(function_name));
		if (where != data_->functions_.end())
			data_->functions_.erase(where);
		else
		{ /* not a duplicate definition */ }

		data_->functions_.insert(StoredFunctions< Arguments >::value_type(function_name, boost::shared_ptr< StoredFunction< Arguments > >(new FunctionPointer2< Arguments >(function, expected_args))));
	}

	void Groovy::installFunction2(const std::string & function_name, const MultiReturnFunction2 & function, int expected_args/* = any__*/, int expected_returns/* = any__*/)
	{
		StoredFunctions< Arguments >::iterator where(data_->functions_.find(function_name));
		if (where != data_->functions_.end())
			data_->functions_.erase(where);
		else
		{ /* not a duplicate definition */ }

		data_->functions_.insert(StoredFunctions< Arguments >::value_type(function_name, boost::shared_ptr< StoredFunction< Arguments > >(new FunctionPointer2< Arguments >(function, expected_args, expected_returns))));
	}

	void Groovy::removeFunction(const std::string & function_name)
	{
		StoredFunctions< Arguments >::iterator where(data_->functions_.find(function_name));
		if (where != data_->functions_.end())
			data_->functions_.erase(where);
		else
		{ /* function not found - no-op */ }
	}
}
