/* Funky: a light-weight embeddable programming language
 * Copyright (c) 2007, Ronald Landheer-Cieslak
 * All rights reserved
 * 
 * This is free software. You may distribute it and/or modify it and
 * distribute modified forms provided that the following terms are met:
 *
 * * Redistributions of the source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer;
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution;
 * * None of the names of the authors of this software may be used to endorse
 *   or promote this software, derived software or any distribution of this 
 *   software or any distribution of which this software is part, without 
 *   prior written permission from the authors involved;
 * * Unless you have received a written statement from Ronald Landheer-Cieslak
 *   that says otherwise, the terms of the GNU General Public License, as 
 *   published by the Free Software Foundation, version 2 or (at your option)
 *   any later version, also apply.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _funky_tests_lazytest_h
#define _funky_tests_lazytest_h

#include <cppunit/extensions/HelperMacros.h>
#include <Funky/Lazy.h>

class LazyTest : public CPPUNIT_NS::TestFixture
{
// Tests registration
	CPPUNIT_TEST_SUITE( LazyTest );
	CPPUNIT_TEST(tryScript01);
	CPPUNIT_TEST(tryScript02);
	CPPUNIT_TEST(tryScript03);
	CPPUNIT_TEST(tryScript04);
	CPPUNIT_TEST(tryScript05);
	CPPUNIT_TEST(tryScript06);
	CPPUNIT_TEST(tryScript07);
	CPPUNIT_TEST(tryScript08);
	CPPUNIT_TEST(tryScript09);
	CPPUNIT_TEST(tryScript10);
	CPPUNIT_TEST(tryScript11);
	CPPUNIT_TEST(tryScript12);
	CPPUNIT_TEST(tryScript13);
	CPPUNIT_TEST(tryScript14);
	CPPUNIT_TEST(tryScript15);
	CPPUNIT_TEST(tryScript16);
	CPPUNIT_TEST(tryScript17);
	CPPUNIT_TEST(tryScript18);
	CPPUNIT_TEST(tryScript19);
	CPPUNIT_TEST(tryScript20);
	CPPUNIT_TEST(tryParseError01);
	CPPUNIT_TEST(tryParseError02);
	CPPUNIT_TEST(tryParseError03);
	CPPUNIT_TEST(tryParseError04);
	CPPUNIT_TEST(tryParseError05);
	CPPUNIT_TEST(tryParseError06);
	CPPUNIT_TEST(tryParseError07);
	CPPUNIT_TEST(tryParseError08);
	CPPUNIT_TEST_SUITE_END();

public:
	virtual void setUp();
	virtual void tearDown();

protected:
	void tryScript01();
	void tryScript02();
	void tryScript03();
	void tryScript04();
	void tryScript05();
	void tryScript06();
	void tryScript07();
	void tryScript08();
	void tryScript09();
	void tryScript10();
	void tryScript11();
	void tryScript12();
	void tryScript13();
	void tryScript14();
	void tryScript15();
	void tryScript16();
	void tryScript17();
	void tryScript18();
	void tryScript19();
	void tryScript20();
	void tryParseError01();
	void tryParseError02();
	void tryParseError03();
	void tryParseError04();
	void tryParseError05();
	void tryParseError06();
	void tryParseError07();
	void tryParseError08();
private:
	Funky::Lazy lazy_;
	int parms_[3];
};

#endif
