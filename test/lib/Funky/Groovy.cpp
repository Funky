/* Funky: a light-weight embeddable programming language
 * Copyright (c) 2007, Ronald Landheer-Cieslak
 * All rights reserved
 * 
 * This is free software. You may distribute it and/or modify it and
 * distribute modified forms provided that the following terms are met:
 *
 * * Redistributions of the source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer;
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution;
 * * None of the names of the authors of this software may be used to endorse
 *   or promote this software, derived software or any distribution of this 
 *   software or any distribution of which this software is part, without 
 *   prior written permission from the authors involved;
 * * Unless you have received a written statement from Ronald Landheer-Cieslak
 *   that says otherwise, the terms of the GNU General Public License, as 
 *   published by the Free Software Foundation, version 2 or (at your option)
 *   any later version, also apply.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "Groovy.h"
#include "Stubs/Functor.h"
#include <Funky/Exceptions/ParseError.h>

CPPUNIT_TEST_SUITE_REGISTRATION( GroovyTest );

void GroovyTest::setUp()
{
	parms_[0] = 0;
	parms_[1] = 1;
	parms_[2] = 2;
}

//! Clean up after the test run.
void GroovyTest::tearDown()
{
}

void GroovyTest::tryScript01(){CPPUNIT_ASSERT(groovy_.eval("(test, 1)"));}
void GroovyTest::tryScript02(){CPPUNIT_ASSERT(groovy_.eval("(add, 1, (neg, 1))") == 0);}
void GroovyTest::tryScript03(){CPPUNIT_ASSERT(groovy_.eval("(add, 1, (neg, @0))", std::vector< int >(parms_, parms_ + 1)) == 1);}
void GroovyTest::tryScript04(){CPPUNIT_ASSERT(groovy_.eval("(add, (neg, (add, 6, 6)), 12)") == 0);}
void GroovyTest::tryScript05(){CPPUNIT_ASSERT(groovy_.eval("(add, (add, 6, 6), -12)") == 0);}
void GroovyTest::tryScript06(){CPPUNIT_ASSERT(groovy_.eval("(!sub-2, (add, @0, (neg, @1)))(sub, 1, 1)") == 0);}
void GroovyTest::tryScript07(){CPPUNIT_ASSERT(groovy_.eval("(!sub, (add, @0, (neg, @1)))(sub, 1, 1)") == 0);}
void GroovyTest::tryScript08(){CPPUNIT_ASSERT(groovy_.eval("(if, 0, 1, 2)") == 2);}
void GroovyTest::tryScript09(){CPPUNIT_ASSERT(groovy_.eval("(if, 1, 1, 2)") == 1);}
void GroovyTest::tryScript10(){CPPUNIT_ASSERT(groovy_.eval("(add, (shift), (add, (shift), (shift)))", std::vector< int >(parms_, parms_ + 3)) == 3);}
void GroovyTest::tryScript11(){CPPUNIT_ASSERT(groovy_.eval("(shift)", std::vector< int >(parms_, parms_ + 3)) == 0);}
void GroovyTest::tryScript12(){CPPUNIT_ASSERT(groovy_.eval("(defined, @3)", std::vector< int >(parms_, parms_ + 3)) == 0);}
void GroovyTest::tryScript13(){CPPUNIT_ASSERT(groovy_.eval("(defined, @2)", std::vector< int >(parms_, parms_ + 3)) == 1);}
void GroovyTest::tryScript14(){CPPUNIT_ASSERT(groovy_.eval("(defined, @1)", std::vector< int >(parms_, parms_ + 3)) == 1);}
void GroovyTest::tryScript15(){CPPUNIT_ASSERT(groovy_.eval("(defined, @0)", std::vector< int >(parms_, parms_ + 3)) == 1);}
void GroovyTest::tryScript16(){CPPUNIT_ASSERT(groovy_.eval("(!sub, (add, @0, (if, (defined, @1), @1, 0)))(sub, 1)") == 1);}
void GroovyTest::tryScript17(){CPPUNIT_ASSERT(groovy_.eval("(shift)", std::vector< int >(parms_ + 1, parms_ + 3)) == 1);}
void GroovyTest::tryScript18(){CPPUNIT_ASSERT(groovy_.eval("(add, @@)", std::vector< int >(parms_ + 1, parms_ + 3)) == 3);}
void GroovyTest::tryScript19()
{
	CPPUNIT_ASSERT(groovy_.eval(
	   "(!sum, "
	   "	(if, (defined, @0),"
	   "		(add,"
	   "			(shift),"
	   "			(if, (defined, @0), (sum, @@), 0)),"
	   "		0"
	   "	)"
	   ")"
	   "(sum, @@)", 
	   std::vector< int >(parms_, parms_ + 3)) == 3);

	for (unsigned int ui(0); ui < 1000; ++ui)
	{
		CPPUNIT_ASSERT(groovy_.run(std::vector< int >(parms_, parms_ + 3)) == 3);
	}
}

void GroovyTest::tryScript20()
{
	groovy_.installFunction("S", Stubs::Functor< std::vector< int > >(), 2, 2);
	CPPUNIT_ASSERT(groovy_.eval("(add, (S, @@))", std::vector< int >(parms_ + 1, parms_ + 3)) == 3);
}

void GroovyTest::tryParseError01()
{
	CPPUNIT_ASSERT_THROW(groovy_.eval("("), Funky::Exceptions::ParseError);
}

void GroovyTest::tryParseError02()
{
	bool caught(false);
	try
	{
		groovy_.eval("(0)");
	}
	catch (const Funky::Exceptions::ParseError & e)
	{
		caught = true;
		CPPUNIT_ASSERT(e.reason_ == Funky::Exceptions::ParseError::function_name_expected__);
	}
	CPPUNIT_ASSERT(caught);
}

void GroovyTest::tryParseError03()
{
	bool caught(false);
	try
	{
		groovy_.eval("(!,0)");
	}
	catch (const Funky::Exceptions::ParseError & e)
	{
		caught = true;
		CPPUNIT_ASSERT(e.reason_ == Funky::Exceptions::ParseError::function_name_expected__);
	}
	CPPUNIT_ASSERT(caught);
}

void GroovyTest::tryParseError04()
{
	bool caught(false);
	try
	{
		groovy_.eval("!,0)");
	}
	catch (const Funky::Exceptions::ParseError & e)
	{
		caught = true;
		CPPUNIT_ASSERT(e.reason_ == Funky::Exceptions::ParseError::left_parenthesis_expected__);
	}
	CPPUNIT_ASSERT(caught);
}

void GroovyTest::tryParseError05()
{
	bool caught(false);
	try
	{
		groovy_.eval("(foo,)");
	}
	catch (const Funky::Exceptions::ParseError & e)
	{
		caught = true;
		CPPUNIT_ASSERT(e.reason_ == Funky::Exceptions::ParseError::literal_parameter_or_statement_expected__);
	}
	CPPUNIT_ASSERT(caught);
}

void GroovyTest::tryParseError06()
{
	bool caught(false);
	try
	{
		groovy_.eval("(foo,0");
	}
	catch (const Funky::Exceptions::ParseError & e)
	{
		caught = true;
		CPPUNIT_ASSERT(e.reason_ == Funky::Exceptions::ParseError::right_parenthesis_expected__);
	}
	CPPUNIT_ASSERT(caught);
}

void GroovyTest::tryParseError07()
{
	bool caught(false);
	try
	{
		groovy_.eval("(!sub-2, (add, @0, (neg, @1)))");
	}
	catch (const Funky::Exceptions::ParseError & e)
	{
		caught = true;
		CPPUNIT_ASSERT(e.reason_ == Funky::Exceptions::ParseError::left_parenthesis_expected__);
	}
	CPPUNIT_ASSERT(caught);
}

void GroovyTest::tryParseError08()
{
	bool caught(false);
	try
	{
		groovy_.eval("(test, 1.0)");
	}
	catch (const Funky::Exceptions::ParseError & e)
	{
		caught = true;
		CPPUNIT_ASSERT(e.reason_ == Funky::Exceptions::ParseError::right_parenthesis_expected__);
	}
	CPPUNIT_ASSERT(caught);
}
