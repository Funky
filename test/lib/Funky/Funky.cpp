/* Funky: a light-weight embeddable programming language
 * Copyright (c) 2007, Ronald Landheer-Cieslak
 * All rights reserved
 * 
 * This is free software. You may distribute it and/or modify it and
 * distribute modified forms provided that the following terms are met:
 *
 * * Redistributions of the source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer;
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the distribution;
 * * None of the names of the authors of this software may be used to endorse
 *   or promote this software, derived software or any distribution of this 
 *   software or any distribution of which this software is part, without 
 *   prior written permission from the authors involved;
 * * Unless you have received a written statement from Ronald Landheer-Cieslak
 *   that says otherwise, the terms of the GNU General Public License, as 
 *   published by the Free Software Foundation, version 2 or (at your option)
 *   any later version, also apply.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "Funky.h"
#include "Stubs/Functor.h"
#include <Funky/Exceptions/ParseError.h>

CPPUNIT_TEST_SUITE_REGISTRATION( FunkyTest );

void FunkyTest::setUp()
{
	parms_[0] = 0;
	parms_[1] = 1;
	parms_[2] = 2;
}

//! Clean up after the test run.
void FunkyTest::tearDown()
{
}

void FunkyTest::tryScript01(){CPPUNIT_ASSERT(funky_.eval("(test, 1)"));}
void FunkyTest::tryScript02(){CPPUNIT_ASSERT(funky_.eval("(add, 1, (neg, 1))") == 0);}
void FunkyTest::tryScript03(){CPPUNIT_ASSERT(funky_.eval("(add, 1, (neg, @0))", std::vector< double >(parms_, parms_ + 1)) == 1);}
void FunkyTest::tryScript04(){CPPUNIT_ASSERT(funky_.eval("(add, (neg, (add, 6, 6)), 12)") == 0);}
void FunkyTest::tryScript05(){CPPUNIT_ASSERT(funky_.eval("(add, (add, 6, 6), -12)") == 0);}
void FunkyTest::tryScript06(){CPPUNIT_ASSERT(funky_.eval("(!sub-2, (add, @0, (neg, @1)))(sub, 1, 1)") == 0);}
void FunkyTest::tryScript07(){CPPUNIT_ASSERT(funky_.eval("(!sub, (add, @0, (neg, @1)))(sub, 1, 1)") == 0);}
void FunkyTest::tryScript08(){CPPUNIT_ASSERT(funky_.eval("(if, 0, 1, 2)") == 2);}
void FunkyTest::tryScript09(){CPPUNIT_ASSERT(funky_.eval("(if, 1, 1, 2)") == 1);}
void FunkyTest::tryScript10(){CPPUNIT_ASSERT(funky_.eval("(add, (shift), (add, (shift), (shift)))", std::vector< double >(parms_, parms_ + 3)) == 3);}
void FunkyTest::tryScript11(){CPPUNIT_ASSERT(funky_.eval("(shift)", std::vector< double >(parms_, parms_ + 3)) == 0);}
void FunkyTest::tryScript12(){CPPUNIT_ASSERT(funky_.eval("(defined, @3)", std::vector< double >(parms_, parms_ + 3)) == 0);}
void FunkyTest::tryScript13(){CPPUNIT_ASSERT(funky_.eval("(defined, @2)", std::vector< double >(parms_, parms_ + 3)) == 1);}
void FunkyTest::tryScript14(){CPPUNIT_ASSERT(funky_.eval("(defined, @1)", std::vector< double >(parms_, parms_ + 3)) == 1);}
void FunkyTest::tryScript15(){CPPUNIT_ASSERT(funky_.eval("(defined, @0)", std::vector< double >(parms_, parms_ + 3)) == 1);}
void FunkyTest::tryScript16(){CPPUNIT_ASSERT(funky_.eval("(!sub, (add, @0, (if, (defined, @1), @1, 0)))(sub, 1)") == 1);}
void FunkyTest::tryScript17(){CPPUNIT_ASSERT(funky_.eval("(shift)", std::vector< double >(parms_ + 1, parms_ + 3)) == 1);}
void FunkyTest::tryScript18(){CPPUNIT_ASSERT(funky_.eval("(add, @@)", std::vector< double >(parms_ + 1, parms_ + 3)) == 3);}
void FunkyTest::tryScript19()
{
	CPPUNIT_ASSERT(funky_.eval(
	   "(!sum, "
	   "	(if, (defined, @0),"
	   "		(add,"
	   "			(shift),"
	   "			(if, (defined, @0), (sum, @@), 0)),"
	   "		0"
	   "	)"
	   ")"
	   "(sum, @@)", 
	   std::vector< double >(parms_, parms_ + 3)) == 3);
}

void FunkyTest::tryScript20()
{
	funky_.installFunction("S", Stubs::Functor< std::vector< double > >(), 2, 2);
	CPPUNIT_ASSERT(funky_.eval("(add, (S, @@))", std::vector< double >(parms_ + 1, parms_ + 3)) == 3);
}

void FunkyTest::tryParseError01()
{
	CPPUNIT_ASSERT_THROW(funky_.eval("("), Funky::Exceptions::ParseError);
}

void FunkyTest::tryParseError02()
{
	bool caught(false);
	try
	{
		funky_.eval("(0)");
	}
	catch (const Funky::Exceptions::ParseError & e)
	{
		caught = true;
		CPPUNIT_ASSERT(e.reason_ == Funky::Exceptions::ParseError::function_name_expected__);
	}
	CPPUNIT_ASSERT(caught);
}

void FunkyTest::tryParseError03()
{
	bool caught(false);
	try
	{
		funky_.eval("(!,0)");
	}
	catch (const Funky::Exceptions::ParseError & e)
	{
		caught = true;
		CPPUNIT_ASSERT(e.reason_ == Funky::Exceptions::ParseError::function_name_expected__);
	}
	CPPUNIT_ASSERT(caught);
}

void FunkyTest::tryParseError04()
{
	bool caught(false);
	try
	{
		funky_.eval("!,0)");
	}
	catch (const Funky::Exceptions::ParseError & e)
	{
		caught = true;
		CPPUNIT_ASSERT(e.reason_ == Funky::Exceptions::ParseError::left_parenthesis_expected__);
	}
	CPPUNIT_ASSERT(caught);
}

void FunkyTest::tryParseError05()
{
	bool caught(false);
	try
	{
		funky_.eval("(foo,)");
	}
	catch (const Funky::Exceptions::ParseError & e)
	{
		caught = true;
		CPPUNIT_ASSERT(e.reason_ == Funky::Exceptions::ParseError::literal_parameter_or_statement_expected__);
	}
	CPPUNIT_ASSERT(caught);
}

void FunkyTest::tryParseError06()
{
	bool caught(false);
	try
	{
		funky_.eval("(foo,0");
	}
	catch (const Funky::Exceptions::ParseError & e)
	{
		caught = true;
		CPPUNIT_ASSERT(e.reason_ == Funky::Exceptions::ParseError::right_parenthesis_expected__);
	}
	CPPUNIT_ASSERT(caught);
}

void FunkyTest::tryParseError07()
{
	bool caught(false);
	try
	{
		funky_.eval("(!sub-2, (add, @0, (neg, @1)))");
	}
	catch (const Funky::Exceptions::ParseError & e)
	{
		caught = true;
		CPPUNIT_ASSERT(e.reason_ == Funky::Exceptions::ParseError::left_parenthesis_expected__);
	}
	CPPUNIT_ASSERT(caught);
}

