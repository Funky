Test_Funky_SRC :=			main.cpp Funky.cpp Groovy.cpp
SRC += $(patsubst %,test/lib/Funky/%,$(Test_Funky_SRC))

Test_Funky_OBJ := $(patsubst %.cpp,test/lib/Funky/%.lo,$(Test_Funky_SRC))

OBJ += $(Test_Funky_OBJ)

TEST_LDFLAGS=libFunky.la $(LIBS) -lcppunit -lltdl
$(eval $(call LINK_BINARY_template,Test_Funky.bin,$(Test_Funky_OBJ),$(TEST_LDFLAGS)))
CHECK_DEPS += Test_Funky.bin 


